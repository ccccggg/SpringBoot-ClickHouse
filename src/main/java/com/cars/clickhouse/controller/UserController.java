package com.cars.clickhouse.controller;

import com.cars.clickhouse.entity.User;
import com.cars.clickhouse.repository.BaseRepository;
import com.cars.clickhouse.repository.UserRepository;
import com.google.common.collect.Lists;
import jakarta.annotation.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author caoqianfan
 * @date 2024/2/8 10:23
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Resource
    private UserRepository userRepository;
    @Resource
    private BaseRepository<User, Long> baseRepository;

    @GetMapping("mock/save")
    public String save() {
        User user = new User().setId(1L).setUsername("admin").setAddr("China1");
        userRepository.save(user);
        return "save success";
    }

    @GetMapping("mock/batch")
    public String batch() {
        long start = System.currentTimeMillis();
        List<User> users = new ArrayList<>();
        User user;
        Timestamp now = new Timestamp(System.currentTimeMillis());
        for (long i = 1; i <= 100000; i++) {
            user = new User().setId(i).setUsername("admin_" + i).setAddr("China_" + i).setCreateTime(now);
            users.add(user);
        }
        //按每500一组分割
        List<List<User>> parts = Lists.partition(users, 500);
        parts.forEach(list -> userRepository.batchInsert(list));
        double total = (double) (System.currentTimeMillis() - start) / 1000;
        return "batch save success, time: " + total + "s";
    }

    @GetMapping("/")
    public List<User> list() {
        return userRepository.findAll();
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable(value = "id") Long id) {
        return userRepository.findById(id).orElse(null);
    }


    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Long id) {
        int i = baseRepository.delById("sys_user", id);
        System.out.printf("影响行数：%d\n", i);
        return "delete success";
    }

    @PostMapping("/saveOrUpdate")
    public User save(User user) {
        return userRepository.save(user);
    }

    @PostMapping("/batch/save")
    public ResponseEntity<String> saveBatch(List<User> users) {
        long start = System.currentTimeMillis();
        //按每500一组分割
        List<List<User>> parts = Lists.partition(users, 500);
        parts.forEach(list -> userRepository.batchInsert(list));
        double total = (double) (System.currentTimeMillis() - start) / 1000;
        return ResponseEntity.ok("batch save success, time: " + total + "s");
    }

    @PutMapping("batch/update")
    public ResponseEntity<String> updateBatch(List<User> users) {
        long start = System.currentTimeMillis();
        //按每500一组分割
        List<List<User>> parts = Lists.partition(users, 500);
        parts.forEach(list -> userRepository.batchUpdate(list));
        double total = (double) (System.currentTimeMillis() - start) / 1000;
        return ResponseEntity.ok("batch save success, time: " + total + "s");
    }
}